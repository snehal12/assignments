import numbers


def size_of_words(llist,mlist):
    print("Using loop =" ,end=" ")
    size=len(llist)
    for i in range(0,size):
        l=len(llist[i])
        mlist.append(l)
    print(mlist)

llist=[]
mlist=[]
m=int(input("enter size"))
print("Enter list")
for i in range(0,m):
    r=input()
    llist.append(r)

size_of_words(llist,mlist)

print("Using map =" ,end=" ")
print(list(map(len,llist)))

print("Using list comprehension =" ,end=" ")
printnum=[len(llist[i]) for i in range(0,len(llist))]
print(printnum)



